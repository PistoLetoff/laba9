Rails.application.routes.draw do
  root 'armstrong#index'
  get 'armstrong/index'
  get 'armstrong/input'
  get 'armstrong/result'
  post 'armstrong/result'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
