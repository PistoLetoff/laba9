require 'rspec'

describe "Test9" do

  before(:each) do
    @driver = Selenium::WebDriver.for :firefox
    @base_url = "https://www.katalon.com/"
    @accept_next_alert = true
    @driver.manage.timeouts.implicit_wait = 30
    @verification_errors = []
  end

  after(:each) do
    @driver.quit
    @verification_errors.should == []
  end

  it "test_9" do
    @driver.find_element(:link, "Armstrong").click
    @driver.find_element(:link, "Let's calculate some of them").click
    @driver.find_element(:id, "armstrong-input").click
    @driver.find_element(:id, "armstrong-input").clear
    @driver.find_element(:id, "armstrong-input").send_keys "0"
    @driver.find_element(:id, "result-btn").click
    (@driver.find_element(:xpath, "(.//*[normalize-space(text()) and normalize-space(.)='Number of digits in number'])[1]/following::div[3]").text).should == "Number of digits have to be positive value!"
  end

  def element_present?(how, what)
    @driver.find_element(how, what)
    true
  rescue Selenium::WebDriver::Error::NoSuchElementError
    false
  end

  def alert_present?()
    @driver.switch_to.alert
    true
  rescue Selenium::WebDriver::Error::NoAlertPresentError
    false
  end

  def verify(&blk)
    yield
  rescue ExpectationNotMetError => ex
    @verification_errors << ex
  end

  def close_alert_and_get_its_text(how, what)
    alert = @driver.switch_to().alert()
    alert_text = alert.text
    if (@accept_next_alert) then
      alert.accept()
    else
      alert.dismiss()
    end
    alert_text
  ensure
    @accept_next_alert = true
  end
end
