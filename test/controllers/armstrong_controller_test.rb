require 'test_helper'

class ArmstrongControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get armstrong_index_url
    assert_response :success
  end

  test "should get input" do
    get armstrong_input_url
    assert_response :success
  end

  test "should get result" do
    get armstrong_result_url
    assert_response :success
  end

end
