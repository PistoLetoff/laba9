class ArmstrongController < ApplicationController
  def index
  end

  def input
  end

  def result
    alert_msg = ''
    notice_msg = ''

    @result = []
    @n = params[:number].to_i

    if @n < 1
      alert_msg = "Number of digits have to be positive value!"
    elsif @n > 39
      notice_msg = "There are no numbers with #{@n} digits"
    elsif @n > 7
      alert_msg = "Very big number of digits. My computer will brake =("
    else
      @result = (10**(@n - 1)).upto(10**@n - 1).select do |num|
        num.digits.reduce(0) {|sum, dig| sum + dig ** @n}.equal? num
      end

      if @result.empty?
        notice_msg = "There are not armstrong numbers with #{@n} digits"
      end
    end

    respond_to do |format|
      format.json do
        render json:
                   {type: @result.class.to_s, value: @result, alert: alert_msg, notice: notice_msg}
      end
    end

  end
end
