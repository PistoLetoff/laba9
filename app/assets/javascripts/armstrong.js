$(".armstrong-input").ready(function(){
    //init_input();
    //console.log('create');
});
$( document ).on('turbolinks:load', function() {
    init_input();
    $("#result-btn").click(function(){
        console.log('clicked');
        $.getJSON('result.json', {number: armstrong_input[0].value}, function(data) {
            upate_table(data.value, data.alert, data.notice)
        });
    })
});
function init_input() {
    armstrong_table_tbody = $("#armstrong-table tbody");
    armstrong_table = $("#armstrong-table");
    info = $("#info");
    armstrong_input = $("#armstrong-input");
}
function upate_table(res, alert_msg, notice_msg) {
    armstrong_table_tbody[0].innerHTML = "";
    info[0].innerHTML = "";
    armstrong_input.removeClass("is-invalid");
    if(res.length > 0) {
        armstrong_table.removeClass("d-none");
        for(let i = 0; i < res.length; i++) {
            let tr = document.createElement("tr");
            let td1 = document.createElement("td");
            td1.innerText = i+1;
            let td2 = document.createElement("td");
            td2.innerText = res[i];
            tr.appendChild(td1);
            tr.appendChild(td2);
            armstrong_table_tbody.append(tr);
        }
    } else {
        if(alert_msg.length > 0) {
            let alert_banner = document.createElement("div");
            alert_banner.setAttribute("class", "alert alert-danger");
            alert_banner.setAttribute("role", "alert");
            alert_banner.innerText = alert_msg;

            info.prepend(alert_banner);

            armstrong_input.addClass("is-invalid");
        }
        if(notice_msg.length > 0) {
            let notice_banner = document.createElement("div");
            notice_banner.setAttribute("class", "alert alert-primary");
            notice_banner.setAttribute("role", "alert");
            notice_banner.innerText = notice_msg;
            info.prepend(notice_banner);
        }
        armstrong_table.addClass("d-none");
    }
}
